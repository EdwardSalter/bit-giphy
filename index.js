require("dotenv").config();

import * as restify from "restify";
import * as builder from "botbuilder";
import * as teamBuilder from "botbuilder-teams";
import fetch, { Headers } from "node-fetch";

const proxy = process.env.HTTP_PROXY;

function getUrl(url) {
  if (!proxy) return url;
  return `${proxy}?${url}`;
}

class App {
  run() {
    const server = restify.createServer();
    let teamChatConnector = new teamBuilder.TeamsChatConnector({
      appId: process.env.MICROSOFT_APP_ID,
      appPassword: process.env.MICROSOFT_APP_PASSWORD
    });

    // Command ID must match what's defined in manifest
    teamChatConnector.onQuery("search", async (event, query, callback) => {
      let response;
      try {
        console.log(query);

        let gifSearchUrl = getUrl("http://api.giphy.com/v1/gifs/trending?api_key=bJ0NUo5JMXEuFFBz2QvtsXdeFY3nZzYt&limit=9&rating=pg");

        if (query.parameters[0].name !== "initialRun") {
          const q = encodeURIComponent(query.parameters[0].value);
          gifSearchUrl = getUrl(`http://api.giphy.com/v1/gifs/search?q=${q}&api_key=bJ0NUo5JMXEuFFBz2QvtsXdeFY3nZzYt&limit=9`);
        }
        console.log(gifSearchUrl)

        const headers = new Headers();
        // headers.set(
        //   "Authorization",
        //   "Basic " +
        //     Buffer.from(
        //       process.env.HTTP_PROXY_USER +
        //         ":" +
        //         process.env.HTTP_PROXY_PASSWORD
        //     ).toString("base64")
        // );
        const gifResponse = await fetch(gifSearchUrl, {
          headers
        });
        console.log(gifResponse)
        const gifs = await gifResponse.json();
        console.log(gifs)
        // Check for initialRun; i.e., when you should return default results
        // if (query.parameters[0].name === 'initialRun') {}

        // Check query.queryOptions.count and query.queryOptions.skip for paging

        // Return auth response
        // let response = teamBuilder.ComposeExtensionResponse.auth().actions([
        //     builder.CardAction.openUrl(null, 'https://authUrl', 'Please sign in')
        // ]).toResponse();

        // Return config response
        // let response = teamBuilder.ComposeExtensionResponse.config().actions([
        //     builder.CardAction.openUrl(null, 'https://configUrl', 'Please sign in')
        // ]).toResponse();

        // Return result response
        response = teamBuilder.ComposeExtensionResponse.result("grid")
          .attachments(
            gifs.data.map(gif => {
              const smallImageUrl =
                getUrl(gif.images.fixed_width_small.url);

              const imageUrl =
                getUrl(gif.images.downsized.url);

              return {
                contentType: "application/vnd.microsoft.card.hero",
                content: {
                  images: [
                    {
                      url: imageUrl,
                      alt: gif.title
                    }
                  ]
                },
                preview: {
                  contentType: "application/vnd.microsoft.card.hero",
                  content: {
                    images: [
                      {
                        url: smallImageUrl,
                        alt: gif.title
                      }
                    ]
                  }
                }
              };
            })
          )
          .toResponse();
        console.log(JSON.stringify(response, null, 2))
      } catch (e) {
        console.error(e);
        callback(e);
        return;
      }
      callback(null, response, 200);
    });

    const listener = teamChatConnector.listen();
    function composeExtension(req, res, next) {
      console.log("POST /api/composeExtension");
      // console.log(req)
      listener(req, res, next);
    }

    server.get("/api/composeExtension", (req, res) =>
      res.send("Try using POST")
    );
    server.post("/api/composeExtension", composeExtension);
    server.get("/", (req, res, next) => {
      res.send("Gifs");
    });

    const port = process.env.PORT || 4444;
    server.listen(port, () => console.log(`listening to port:` + port));
  }
}

const app = new App();
app.run();
